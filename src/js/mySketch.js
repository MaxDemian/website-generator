let allParticles = [];
let bgParticles =[];
let resBG = 800;
let maxLevel = 2;
let useFill = false;
let useFillBG = false;

let isScriptLoaded = false;
let data = [];
let bgdate =[];
let canvas;
let bgcolor, tricolor, strokecolor;

function setup() {
  // set up canvas
  canvas = createCanvas(window.innerWidth, window.innerHeight);
  //canvas.position(0, 0); //defined ins Style.css
  canvas.class("bg");
  canvas.style('z-index','-1');

  bgcolor = colorAlpha('#ffffff', 0.25);//#041f2d
  tricolor = colorAlpha('#3293d1',0.75);
  strokecolor = colorAlpha('#4c95be', 0.85);
  /*for (var i = 0; i < resBG; i++) {
    bgParticles.push(new Particle(random(-50, width + 50), random(-50, height + 50), random(0,maxLevel)));
  }

  if (bgParticles.length > 0) {
    // Run script to get points to create triangles with.
    bgdata = Delaunay.triangulate(bgParticles.map(function(pt) {
      return [pt.pos.x, pt.pos.y];
  }));
  // Display triangles individually
  }*/

}

function draw() {
  // Create fade effect.
  noStroke();
  fill(bgcolor);
  rect(0, 0, width, height);
  /*if (bgParticles.length > 0) {
    displayBG();
  }*/

  // Move and spawn particles.
  // Remove any that is below the velocity threshold.
  for (var i = allParticles.length-1; i > -1; i--) {
    allParticles[i].move();

    if (allParticles[i].vel.mag() < 0.02) {
      allParticles.splice(i, 1);
    }
  }

  if (allParticles.length > 0) {
    // Run script to get points to create triangles with.
    data = Delaunay.triangulate(allParticles.map(function(pt) {
      return [pt.pos.x, pt.pos.y];
    }));

    // Display triangles individually.
    for (let i = 0; i < data.length; i += 3) {
      // Collect particles that make this triangle.
      let p1 = allParticles[data[i]];
      let p2 = allParticles[data[i+1]];
      let p3 = allParticles[data[i+2]];

      // Don't draw triangle if its area is too big.
      let distThresh = 80;

      if (dist(p1.pos.x, p1.pos.y, p2.pos.x, p2.pos.y) > distThresh) {
        continue;
      }

      if (dist(p2.pos.x, p2.pos.y, p3.pos.x, p3.pos.y) > distThresh) {
        continue;
      }

      if (dist(p1.pos.x, p1.pos.y, p3.pos.x, p3.pos.y) > distThresh) {
        continue;
      }

      // Base its hue by the particle's life.
      push()
      colorMode(HSB,360);
      if (useFill) {
        noStroke();
        fill(165+p1.life*1.5, 360, 360);
      } else {
        noFill();
        strokeWeight(0.25);
        stroke(165+p1.life*1.5, 360, 360);
      }

      triangle(p1.pos.x, p1.pos.y,
               p2.pos.x, p2.pos.y,
               p3.pos.x, p3.pos.y);
      pop();
    }
  }

}

function mouseMoved() {
  allParticles.push(new Particle(mouseX, mouseY, maxLevel));
}

// Helper-Functions
//======================================
// Moves to a random direction and comes to a stop.
// Spawns other particles within its lifetime.
function Particle(x, y, level) {
  this.level = level
  this.life = 0;

  this.pos = new p5.Vector(x, y);
  this.vel = p5.Vector.random2D();
  this.vel.mult(map(this.level, 0, maxLevel, 5, 2));

  this.move = function() {
    this.life++;

    // Add friction.
    this.vel.mult(0.9);

    this.pos.add(this.vel);

    // Spawn a new particle if conditions are met.
    if (this.life % 10 == 0) {
      if (this.level > 0) {
        this.level -= 1;
        let newParticle = new Particle(this.pos.x, this.pos.y, this.level-1);
        allParticles.push(newParticle);
      }
    }
  }
}

/*function displayBG(){
    for (let i = 0; i < bgdata.length; i += 3) {
    // Collect particles that make this triangle.
    let p1 = bgParticles[bgdata[i]];
    let p2 = bgParticles[bgdata[i+1]];
    let p3 = bgParticles[bgdata[i+2]];

    push()
    if (useFillBG) {
      noStroke();
      fill(tricolor);
    } else {
      noFill();
      strokeWeight(0.15);
      stroke(tricolor);
    }

    let distThresh = 100;

    if (dist(p1.pos.x, p1.pos.y, p2.pos.x, p2.pos.y) > distThresh) {
      continue;
    }

    if (dist(p2.pos.x, p2.pos.y, p3.pos.x, p3.pos.y) > distThresh) {
      continue;
    }

    if (dist(p1.pos.x, p1.pos.y, p3.pos.x, p3.pos.y) > distThresh) {
      continue;
    }

    triangle(p1.pos.x, p1.pos.y,
             p2.pos.x, p2.pos.y,
             p3.pos.x, p3.pos.y);
    pop();
  }
}*/

//Colorconversion
function colorAlpha(aColor, alpha) {
  let c = color(aColor);
  return color('rgba(' + [red(c), green(c), blue(c), alpha].join(',') + ')');
}

// Changed Browserwindow
//======================================
function windowResized() {
  location.reload();
}
