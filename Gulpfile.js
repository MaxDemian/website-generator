const gulp = require('gulp');

const paths = {
  src: 'src/**/*',
  srcHTML: 'src/html/*.html',
  srcCSS: 'src/css/*.css',
  srcJS: 'src/js/*.js',
  srcNJ:'src/templates/**/**/*.nunjucks',
  srcNJIndex:'src/templates/pages/index/*.nunjucks',
  srcNJProject:'src/templates/pages/projectmp/*.nunjucks',
  srcTempProject:'src/templates/pages/projectmp/',
  srcNJParts:'src/templates/pages/',
  srcRes: 'src/resources/',
  srcAss: 'src/assets/*.nunjucks',
  srcTile: 'src/tiles/*.*',
  srcProj: 'src/projects/*/*.*',
  srcProjMd: 'src/projects/', //doppelt mit load TODO
  loadTile: 'src/tiles',
  loadProjects: 'src/projects',
  srcLib:'src/libs/*.*',

  tmp: 'tmp',
  tmpIndex: 'tmp/index.html',
  tmpCSS: 'tmp/css/',
  tmpJS: 'tmp/js/',
  tmpProj: 'tmp/projects',
  tmpAss: 'tmp/assets',
  tmpTile: 'tmp/tiles',
  tmpLib:'tmp/libs',

  dist: 'dist',
  distIndex: 'dist/index.html',
  distCSS: 'dist/**/*.css',
  distJS: 'dist/**/*.js',
};

//var inject = require('gulp-inject');
const webserver = require('gulp-webserver');
const data = require('gulp-data');
const gulpSequence = require('gulp-sequence');
const imagemin = require('gulp-imagemin');
const del = require('del');
const template = require('gulp-template');
const rename = require("gulp-rename");
const addsrc = require('gulp-add-src');

const render = require('gulp-nunjucks-render');
const markdown = require('nunjucks-markdown');
const marked = require('marked');


const fs = require("fs-extra");
const dirToJson = require('dir-to-json');
const sortJsonArray = require('sort-json-array');
const log = require('fancy-log');

marked.setOptions({
  renderer: new marked.Renderer(),
  gfm: true,
  tables: true,
  breaks: true,
  pendantic: true,
  sanitize: false,
  smartLists: true,
  smartypants: true
});

var manageEnvironment = function(env) {
  markdown.register(env, marked)
};


///- Create JSONs from Folders and manipulate them for website structure and layout

  function dirjson(filepath,filename) {
  let finaljson;

  dirToJson(filepath,true)
    .then( function( dirTree ){
    for (let i = 0;i<dirTree.children.length; i++) {
      if (filename == "tiles.json") {
        delete dirTree.children[i].parent;
        dirTree.children[i].path="./tiles/"+dirTree.children[i].name;
        dirTree.children[i].id=dirTree.children[i].name.substring(0,2);
        dirTree.children[i].title=dirTree.children[i].name.substring(3,dirTree.children[i].name.indexOf("_",3));
        dirTree.children[i].link="./projects/" + dirTree.children[i].title + ".html";
        let neighbours = [false,false,false,false,false,false];
        let counter = 0;
        if (dirTree.children[i].name.includes("rim")) {
          dirTree.children[i].class="rim";
        }else if (dirTree.children[i].name.includes("edge")) {
          dirTree.children[i].class="edge";
          ///- Bestimmung der Nachbarn und festlegen des Verlaufs
          if (i>12 && i<71) {
            if (i%2 == 0) {
              if (dirTree.children[i-12].name.includes("image")) {
                neighbours[0]= true;
                dirTree.children[i].gradient="to bottom,black 0%, white 45%"; // black 0%, black 15% / #16222a, #3a6073 / #000000 0%, #434343 25%
              }
              if (dirTree.children[i+1].name.includes("image")) {
                neighbours[1]= true;
                dirTree.children[i].gradient="to left bottom,black 0%, white 45%";
              }
              if (dirTree.children[i+13].name.includes("image") ) {
                neighbours[2]= true;
                dirTree.children[i].gradient="to left top,black 0%, white 45%";
              }
              if (dirTree.children[i+12].name.includes("image")) {
                neighbours[1]= true;
                dirTree.children[i].gradient="to top,black 0%, white 45%";
              }
              if (dirTree.children[i+11].name.includes("image")) {
                neighbours[1]= true;
                dirTree.children[i].gradient="to right top,black 0%, white 45%";
              }
              if (dirTree.children[i-1].name.includes("image")) {
                neighbours[1]= true;
                dirTree.children[i].gradient="to right bottom,black 0%, white 45%";
              }
            }else {
              if (dirTree.children[i-12].name.includes("image")) {
                neighbours[0]= true;
                dirTree.children[i].gradient="to bottom,black 0%, white 45%"; // black 0%, black 15% / #16222a, #3a6073 / #000000 0%, #434343 25%
              }
              if (dirTree.children[i-11].name.includes("image")) {
                neighbours[1]= true;
                dirTree.children[i].gradient="to left bottom,black 0%, white 45%";
              }
              if (dirTree.children[i+1].name.includes("image")) {
                neighbours[2]= true;
                dirTree.children[i].gradient="to left top,black 0%, white 45%";
              }
              if (dirTree.children[i+12].name.includes("image")) {
                neighbours[1]= true;
                dirTree.children[i].gradient="to top,black 0%, white 45%";
              }
              if (dirTree.children[i-1].name.includes("image")) {
                neighbours[1]= true;
                dirTree.children[i].gradient="to right top,black 0%, white 45%";
              }
              if (dirTree.children[i-13].name.includes("image")) {
                neighbours[1]= true;
                dirTree.children[i].gradient="to right bottom,black 0%, white 45%";
              }
            }
          }
          neighbours.forEach(v => v ? counter++ : v);
          if (counter > 1) {
            dirTree.children[i].gradient="to left top,#000000, #000000";
          } else if (counter == 0) {
            dirTree.children[i].gradient="to left top,white,white";
          }

        }else{
          dirTree.children[i].class="content";
        }
      }else{
            dirTree.children[i].path="./"+dirTree.children[i].path;
            let project = dirTree.children[i];
            let projectname = project.name;
            for (let j = 0;j<project.children.length; j++) {
              project.children[j].id=project.children[j].name.substring(0,2);
              if (project.children[j].name.includes("title")) {
                project.children[j].type="title";
                project.children[j].path= project.children[j].path;
                log(project.children[j].path);
              }
              else if (project.children[j].name.includes(".md")) {
                project.children[j].type="markdown";
                project.children[j].path= project.children[j].path;
                log(project.children[j].path);
              }else if (project.children[j].name.includes("mainimage")){
                project.children[j].type="mainimg";
                project.children[j].path="./"+project.children[j].path;
              }
              else if (project.children[j].name.includes(".png")){
                project.children[j].type="img";
                project.children[j].path="./"+project.children[j].path;
              }
            }
      }
    }

    if (filename == "tiles.json") {
      finaljson = {"children" : sortJsonArray(dirTree.children,'id')};
    }else {
      finaljson = dirTree;
    }

     fs.writeJson(paths.srcRes+filename,finaljson)
     .then(() => {
      console.log( finaljson );
      console.log("File has been created");
      })
    .catch(err => {
      console.error(err)
    });
    })
    .catch( function( err ){
    throw err;
    });
}

gulp.task('dirtojson', function() {
      dirjson(paths.loadTile, "tiles.json");
});

gulp.task('dirtojsonpages', function() {
      dirjson(paths.loadProjects,"projects.json");
});

/// Create Templates for pages

gulp.task('template', function () {
    var json = require("./"+paths.srcRes+"projects.json");
    for (let i = 0;i < json.children.length; i++) {
      gulp.src(paths.srcAss)
      .pipe(data(() => ({num: i})))
      .pipe(template())
      .pipe(gulp.dest(paths.srcTempProject))
      .pipe(addsrc.prepend(paths.srcTempProject+"project.nunjucks"))
      .pipe(rename(json.children[i].name+".nunjucks"))
      .pipe(gulp.dest(paths.srcTempProject));
    }
});


gulp.task('copycss', function () {
  return gulp.src(paths.srcCSS).pipe(gulp.dest(paths.tmpCSS));
});

gulp.task('copyjs', function () {
  return gulp.src(paths.srcJS).pipe(gulp.dest(paths.tmpJS));
});

gulp.task('copyass', function () {
  return gulp.src(paths.srcAss).pipe(gulp.dest(paths.tmpAss));
});

gulp.task('copytile', function () {
  gulp.src(paths.srcTile)
        .pipe(imagemin())
        .pipe(gulp.dest(paths.tmpTile));
});

gulp.task('copyproject', function () {
  gulp.src(paths.srcProj)
        .pipe(imagemin())
        .pipe(gulp.dest(paths.tmpProj));
});

gulp.task('copylibs', function ()
{
  return gulp.src(paths.srcLib).pipe(gulp.dest(paths.tmpLib));
});

gulp.task('copytmp', function(callback) {
  gulpSequence('clean',['copylibs','copyass','copytile','copyproject'])(callback)
});

gulp.task('nunjucks', function() {
  return gulp.src(paths.srcNJIndex)

  .pipe(data(function() {
  return require("./"+paths.srcRes+"tiles.json")
  }))

  .pipe(render({
      path: [paths.srcNJParts]
    }))
  .pipe(gulp.dest(paths.tmp))
});

gulp.task('nunjuckspages', function() {
  return gulp.src(paths.srcNJProject)

  .pipe(data(function() {
  return require("./"+paths.srcRes+"projects.json")
  }))

  .pipe(render({
      path: [paths.srcNJParts,paths.srcProjMd],
      envOptions: {
          autoescape: false
      },
      manageEnv: manageEnvironment,
    }))
  .pipe(gulp.dest(paths.tmpProj))
});


gulp.task('buildtmp', function(callback) {
  gulpSequence('clean', 'copytmp','buildsrc')(callback)
});

gulp.task('buildsrc', function(callback) {
  gulpSequence('dirtojsonpages','dirtojson','template','copycss', 'copyjs', 'nunjucks','nunjuckspages')(callback)
});

gulp.task('watch', function () {
    //livereload.listen();
    gulp.watch(paths.src, ['buildsrc']);
});

gulp.task('serve', function () {
  return gulp.src(paths.tmp)
    .pipe(webserver({
      port: 3000,
      livereload: true
    }));
});

// CLEAN files
gulp.task('clean', function () {
    return del(paths.tmp);
});
